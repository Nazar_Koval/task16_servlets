package com.koval.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
            writer.println("<html><body style=\"font-family: Arial;\n"
                    + "font-size: 12pt;\n"
                    + "background-color: #03fc94;\">");
            writer.println("<h1>Pizza selection</h1>");
            writer.println("<h3>Select</h3>");
            writer.println("<form action='clients' method='Post'> \n"
                    + "<select id='pizza-type' name='pizza-type' method='Post'>\n"
                    + "<option value='Cheese'>Cheese</option>\n"
                    + "<option value='Pepperoni'>Pepperoni</option>\n"
                    + "<option value='Clam'>Clam</option>\n"
                    + "<option value='Veggie'>Veggie</option>\n"
                    + "</select>\n"
                    + "<button type='submit'>Add to order</button>\n"
                    + "</form>");
            writer.println("<p><a href='menu'>Back to the menu</a></p>");
            writer.println("<p><a href='clients'>View your order</a></p>");
            writer.println("<p><a href='delete'>Delete a pizza from your order</a></p>");
            writer.println("<p><a href='update'>Change your order</a></p>");
            writer.println("</body></html>");
        }finally {
            assert writer!=null;
            writer.close();
        }
    }
}
