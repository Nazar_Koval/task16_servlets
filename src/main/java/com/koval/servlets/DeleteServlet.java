package com.koval.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {

    //HTTP 405 ERROR STILL DON`T KNOW HOW TO FIX
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
            writer.println("<html><head><body>");
            writer.println("<h2>Pizza deletion</h2>");
            if (ClientServlet.orderList.size() == 0) {
                writer.println("<h3>Your order is empty</h3>");
            } else {
                writer.println("<form action='clients'>\n"
                        + "<p> ID: <input type='number' min='1' name='order_id'>\n"
                        + "<input type='button' onclick='remove(this.form.order_id.value)' name='button' value='delete'>\n"
                        + "</p>\n"
                        + "</form>");
                writer.println("<script type='text/javascript'>\n"
                        + "function remove(id){fetch('clients/' +id,{method:'DELETE'});}\n"
                        + "</script>");
                writer.println("<p><a href='order'>Add a new pizza to your order</a></p>");
                writer.println("<p><a href='update'>Change your order</a></p>");
                writer.println("<p><a href='menu'>Back to the menu</a></p>");
                writer.println("</body></html>");
            }
        }finally {
            assert writer!=null;
            writer.close();
        }
    }
}
