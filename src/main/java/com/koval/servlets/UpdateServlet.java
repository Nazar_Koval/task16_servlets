package com.koval.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet("/update")
public class UpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
            writer.println("<html><body style=\"font-family: Arial;\n"
                    + "font-size: 12pt;\n"
                    + "background-color: #fc03c6;\">");
            writer.println("<form method='Post'>\n"
                    + "Order number: <input type='number' min='1' name='id'>"
                    + "<h3>Select any toppings to your pizza</h3>\n"
                    + "<p><input type='checkbox' name='toppings' value='Tomatoes'>Tomatoes</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Parmesan'>Parmesan</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Chicken'>Chicken</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Mozzarella'>Mozzarella</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Beef'>Beef</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Tune'>Tune</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Pepper'>Pepper</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Pork'>Pork</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Sweet paprika'>Sweet paprika</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Pineapple'>Pineapple</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Salami'>Salami</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Mushrooms'>Mushrooms</p>\n"
                    + "<p><input type='checkbox' name='toppings' value='Eggs'>Eggs</p>\n"
                    + "<input type='submit' value='Send changes to chef'>\n"
                    + "</form>");
            writer.println("<p><a href='order'>Add a new pizza to your order</a></p>");
            writer.println("<p><a href='delete'>Delete a pizza from your order</a></p>");
            writer.println("<p><a href='menu'>Back to the menu</a></p>");
            writer.println("</body></html>");
        }finally {
            assert writer != null;
            writer.close();
        }
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) {
        int orderNumber = Integer.parseInt(req.getParameter("id"));
        String[] toppings = req.getParameterValues("toppings") ;
        ClientServlet.orderList.get(orderNumber-1).getToppings().addAll(Arrays.asList(toppings));
    }
}
