package com.koval.servlets;

import com.koval.pizza.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@WebServlet("/clients")
public class ClientServlet extends HttpServlet {

   public static List<Pizza> orderList = new LinkedList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
            writer.println("<html><body style=\"font-family: Arial;\n"
                    + "font-size: 12pt;\n"
                    + "background-color: #62fc03;\">");
            writer.println("<h2>Your current order<h2>");
            if (orderList.size() == 0) {
                writer.println("<h3>Your order is empty</h3>");
            } else {
                for (Pizza pizza : orderList) {
                    writer.println("<p>" + pizza.info() + "</p>");
                }
            }
            writer.println("<p><a href='order'>Add a new pizza to your order</a></p>");
            writer.println("<p><a href='delete'>Delete a pizza from your order</a></p>");
            writer.println("<p><a href='update'>Change your order</a></p>");
            writer.println("<p><a href='menu'>Back to the menu</a></p>");
            writer.println("</body></html>");
        }finally {
            assert writer!=null;
            writer.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("pizza-type");
        Pizza ordered = null;
        switch (type){
            case "Pepperoni":
                ordered = new Pepperoni();
                break;
            case "Cheese":
                ordered = new Cheese();
                break;
            case "Veggie":
                ordered = new Veggie();
                break;
            case "Clam":
                ordered = new Clam();
                break;
            default:
                break;
        }
        orderList.add(ordered);
        doGet(req,resp);
    }

    //Fix needed
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int orderNumber = Integer.parseInt(req.getParameter("id"));
        String[] toppings = req.getParameterValues("toppings") ;
        ClientServlet.orderList.get(orderNumber-1).getToppings().addAll(Arrays.asList(toppings));
        doPost(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getRequestURI();
        id = id.replace("/task16-1.0-SNAPSHOT/clients?order_id","");
        orderList.remove(Integer.parseInt(id));
        doGet(req,resp);
    }
}