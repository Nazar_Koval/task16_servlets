package com.koval.pizza;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Cheese implements Pizza {

    private String dough = "thick";
    private String sauce = "Pesto";
    private int radius = 30;
    private List<String> toppings = new LinkedList<>(Arrays.asList("Cheddar",
            "Parmesan", "Provolone", "Mozzarella"));

    @Override
    public String getDough() {
        return this.dough;
    }

    @Override
    public void setDough(String dough) {
        this.dough = dough;
    }

    @Override
    public String getSauce() {
        return this.sauce;
    }

    @Override
    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    @Override
    public List<String> getToppings() {
        return this.toppings;
    }

    @Override
    public void setToppings(List<String> toppings) {
        this.toppings = toppings;
    }

    @Override
    public int getRadius() {
        return this.radius;
    }

    @Override
    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String info() {
        return "Cheese pizza of " + radius + "sm radius with "
                + dough + " dough, "
                + sauce + " sauce and "
                + toppings.toString().replaceAll("\\[", "").replaceAll("]","");
    }
}
